let content = document.querySelector("#content");
let compass = document.querySelector("#compass");
let bars_quantity = document.querySelector("#bars");
let options = document.querySelector("#options");
let getChart_bt = document.querySelector("#chart");
let fieldset = document.querySelector('#values');

getChart_bt.addEventListener('click', printChart);
bars_quantity.addEventListener('change', () => {
    if (fieldset.innerHTML != "") {
        fieldset.innerHTML = "";
    }
    if (bars_quantity.value != "") {
        for (var i = 0; i < Number(bars_quantity.value); i++) {
            let values = document.createElement('input');
            values.type = "number";
            values.name = "bar " + String(i + 1);
            values.maxLength = "2";
            values.style.width = " 145px";
            values.max = "10";
            values.min = "0";
            values.addEventListener('focusout', () => {
                if (values.valueAsNumber > 10) {
                    values.value = "10";
                }
                else if (values.valueAsNumber < 0) {
                    values.value = "0";
                }
                else if (values.valueAsNumber < 10 && values.valueAsNumber > 0) {
                }
                else if (values.value == "") {
                    values.placeholder = "Invalid Entry!";
                    values.value = "";
                }
            })
            values.addEventListener('focusin', () => {
                values.placeholder = "Set a value to " + values.name;
            })
            values.placeholder = "Set a value to " + values.name;

            values.id = String(i);
            values.style.margin = "auto";
            values.style.marginBottom = "5px";
            fieldset.append(values);
        }
    }
});
// Printing chart on screen...
function printChart() {
    if (allRight()) {
        content.innerHTML = "";
        let compass_value = compass.value;
        let inputs = fieldset.getElementsByTagName('input');
        let input_values = new Array(inputs.length);
        let input_names = new Array(inputs.length);
        for (var i = 0; i < input_values.length; i++) {
            input_values[i] = Number(inputs[i].value);
            input_names[i] = inputs[i].name;
        }
        let chart = createChart(compass_value, input_values, input_names);
        let filledChart = fillChart(chart, input_values);
        content.append(filledChart);
    } else {
        window.alert('Put all the necessary information!')
    }

}
// Create a table chart...
function createChart(compass_value, input_values, input_names) {
    let table = document.createElement('table');
    table.style.height = "550px";
    table.style.width = "550px";
    // Building a vertical chart...
    if (compass_value == 'V') {
        let tr_number = (input_values.slice()).sort((a, b) => {
            return b - a;
        })[0] + 1;
        let td_number = input_names.length;
        for (var i = 0; i < tr_number + 1; i++) {
            let tr = document.createElement('tr');
            for (var j = 0; j < td_number + 1; j++) {
                let td = document.createElement('td');
                td.style.padding = "0px";
                td.style.backgroundColor = "white";
                // Verifying cell type...
                if (j == 0) {
                    if (i != tr_number) {
                        td.innerText = String(tr_number - i);
                        td.style.borderRight = "none";
                        td.style.borderBottom = "2px solid black";
                        td.style.verticalAlign = "top";
                        td.style.width = "25px";
                        if (i + 1 == tr_number) {
                            td.style.borderBottom = "none";
                        }
                        td.classList.add("numHeader");
                    } else {
                        td.style.borderTop = "2px solid black";
                        td.classList.add('blankTd');
                    }
                } else {
                    if (i == tr_number) {
                        td.innerText = input_names[j - 1];
                        td.style.borderTop = "2px solid black";
                        td.style.borderLeft = "2px solid black";
                        td.style.height = "50px";
                        td.classList.add("nameHeader");
                    } else {
                        td.classList.add('valueCell');
                        let td_format = String(Number(table.style.width.replace('px', '') - 28) / td_number) + "px";
                        td.style.width = String(Number(td_format.replace('px', '')) / 3 * 2) + "px";
                        let padding_side = String((Number(td_format.replace('px', '')) - Number(td.style.width.replace('px', ''))) / 2) + "px";
                        td.name = String(tr_number - i);
                        td.style.paddingLeft = padding_side;
                        td.style.paddingRight = padding_side;
                        td.classList.add(String(input_names[j - 1]).replace(' ', '_'));
                    }
                }
                //Building table rows...
                tr.append(td);
            }
            //Building table...
            table.append(tr);
        }
        // Building a horizontal chart...
    } else {
        let td_number = (input_values.slice()).sort((a, b) => {
            return b - a;
        })[0] + 1;
        let tr_number = input_names.length;
        let td_width = String((table.style.width.replace('px', '') - 50) / (td_number)) + "px";
        for (var i = 0; i < tr_number + 1; i++) {
            let tr = document.createElement('tr');
            for (var j = 0; j < td_number + 1; j++) {
                let td = document.createElement('td');
                td.style.padding = "0px";
                // Verifying cell type...
                if (j == 0) {
                    if (i != tr_number) {
                        td.innerText = input_names[i];
                        td.style.borderTop = "none";
                        td.style.borderLeft = "none";
                        td.style.width = "50px";
                        td.style.borderBottom = "2px solid black";
                        td.style.borderRight = "2px solid black";
                        td.classList.add("nameHeader");
                    } else {
                        td.classList.add('blankTd');
                        td.style.borderTop = "none";
                        td.style.borderRight = "2px solid black";
                    }
                } else {
                    if (i == tr_number) {
                        td.style.width = td_width;
                        td.innerText = String(j);
                        td.style.borderTop = "none";
                        td.style.borderRight = "none";
                        td.style.borderLeft = "2px solid black";
                        td.style.height = "25px";
                        td.style.textAlign = "right";
                        if (j == 1) {
                            td.style.borderLeft = "none";
                        }
                        td.classList.add("numHeader");
                    } else {
                        td.classList.add('valueCell');
                        let td_format = String(Number(table.style.height.replace('px', '') - 25) / tr_number) + "px";
                        td.style.height = String(Number(td_format.replace('px', '')) / 3 * 2) + "px";
                        let side_padding = String((Number(td_format.replace('px', '')) - Number(td.style.height.replace('px', ''))) / 2) + "px";
                        td.name = String(j);
                        td.style.paddingTop = side_padding;
                        td.style.paddingBottom = side_padding;
                        td.classList.add(String(input_names[i]).replace(' ', '_'));
                    }
                }
                //Building table rows...
                tr.append(td);
            }
            //Building table...
            table.append(tr);
        }
    }
    // Setting table orientation...
    table.name = compass_value;
    return table;
}
// Filling table...
function fillChart(table, input_values) {
    let cells = table.getElementsByClassName('valueCell');
    let cells_name = table.getElementsByClassName('nameHeader');
    let max_value = (input_values.slice()).sort((a, b) => {
        return b - a;
    })[0];
    for (var i = 0; i < cells_name.length; i++) {
        // Filling a vertical table...
        if (table.name == 'V') {
            if (input_values[i] != 0) {
                let fillAll = false;
                let firstTime = true;
                for (var j = 0; j <= max_value; j++) {
                    let p = document.createElement('p');
                    p.innerText = "|";
                    p.style.margin = '0';
                    p.style.textAlign = 'center';
                    p.style.padding = '0';
                    p.style.height = '100%';
                    p.style.width = '100%';
                    p.style.paddingTop = '2px';
                    p.style.fontSize = '25px';
                    if (cells[i + (j * cells_name.length)].name == input_values[i]) {
                        fillAll = true;
                    }
                    if (fillAll) {
                        if (firstTime) {
                            p.innerText = input_values[i];
                            firstTime = false;
                        }
                        p.classList.add('bar_piece');
                    } else {
                        p.style.color = 'white';
                    }
                    cells[i + (j * cells_name.length)].append(p);
                }
            }
            // Filling a horizontal table...        
        } else {
            if (input_values[i] != 0) {
                let fillAll = true;
                for (var j = 0; j <= max_value; j++) {
                    console.log(cells[j + (i * cells_name.length)].name);
                    let p = document.createElement('p');
                    p.innerText = "---";
                    p.style.display = 'grid';
                    p.style.alignItems = 'center';
                    p.style.margin = '0';
                    p.style.padding = '0';
                    p.style.height = '100%';
                    p.style.width = '100%';
                    p.style.textAlign = 'right';
                    p.style.paddingRight = '1px';
                    p.style.fontSize = '25px';
                    if (cells[j + (i * (max_value + 1))].name == input_values[i]) {
                        p.style.textAlign = 'right';
                        p.style.alignItems = 'center';
                        p.innerText = input_values[i];
                        fillAll = false;
                    }
                    if (fillAll || p.innerText == input_values[i]) {
                        p.classList.add('bar_piece');
                    } else {
                        p.style.color = 'white';
                    }
                    cells[j + (i * (max_value + 1))].append(p);
                }
            }
        }
    }
    return table;
}

function allRight() {
    let value;
    compass.value != "" && bars_quantity.value != "" ? value = true : value = false;
    if (!value) {
        return value;
    } else {
        let inputs = fieldset.getElementsByTagName('input');
        for (input of inputs) {
            if (input.value == "") {
                value = false;
                break;
            }
        }
        return value;
    }
}